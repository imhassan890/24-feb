
const Post = require("../models/Post");

const userdata = {
//CREATE POST
async createPost (req, res) {
  const newPost = new Post(req.body);
  try {
    const savedPost = await newPost.save();
    res.status(200).json(savedPost);
  } catch (err) {
    res.status(500).json(err);
  }
},

//UPDATE POST
async updatePost (req, res) {
    try {
      const post = await Post.findById(req.params.id);
        try {
          const updatedPost = await Post.findByIdAndUpdate(
            req.params.id,
            {
              $set: req.body,
            },
            { new: true }
          );
          res.status(200).json(updatedPost);
        } catch (err) {
          res.status(500).json(err);
        }
     
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //DELETE POST
 async deletePost (req, res) {
    try {
      const post = await Post.findById(req.params.id);
        try {
          await post.delete();
          res.status(200).json("Post has been deleted...");
        } catch (err) {
          res.status(500).json(err);
        }
      
    } catch (err) {
      res.status(500).json(err);
    }
  },

  //GET POST
async getPost(req, res){
    try {
      const post = await Post.findById(req.params.id);
      res.status(200).json(post);
    } catch (err) {
      res.status(500).json(err);
    }
  }
}
module.exports = userdata ; 