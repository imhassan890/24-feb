const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const postRoute = require("./router/posts");

dotenv.config();
port = process.env.PORT
app.use(express.json());
mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,

  }).then(console.log("Connect to MongoDB"))
    .catch((err) => console.log(err));

    app.use("/api/posts", postRoute);

app.listen(port,()=>{
    console.log("backend is running on port "+ port);
})